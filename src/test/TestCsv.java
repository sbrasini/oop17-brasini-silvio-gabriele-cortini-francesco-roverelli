package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import controller.CsvReadWrite;
import controller.CsvReadWriteImpl;
import model.DatabaseImpl;

/**
 * JUnit tests for CsvReadWrite.
 * 
 * @author silviobrasini
 *
 */
public class TestCsv {

    private static final int SIZE = 500;

    /**
     * Insert 500 elements into the csv file.
     */
    public void testWriteCsv() {
        final CsvReadWrite csv = new CsvReadWriteImpl();
        for (int x = 0; x < SIZE; x++) {
            csv.writeCsv(new DatabaseImpl("C://", "a.db", "test", "passw", "postgresql", "database nr. " + x));
        }
        List<String> listDB = new ArrayList<>();
        listDB = csv.readCSV();
        assertEquals(listDB.size(), SIZE);
    }

    /**
     * Check if the 500 elements were inserted correctly.
     */
    public void testGetDatabase() {
        final CsvReadWrite csv = new CsvReadWriteImpl();
        List<String> list = new ArrayList<>();
        for (int x = 0; x < SIZE; x++) {
            list = csv.getSelectedDatabase("database nr. " + x);
            assertEquals(list.get(0), "a.db");
            assertEquals(list.get(1), "C://");
            assertEquals(list.get(2), "test");
            assertEquals(list.get(3), "passw");
            assertEquals(list.get(4), "postgresql");
            assertEquals(list.get(5), "database nr. " + x);
        }
    }

    /**
     * Delete all the 500 elements.
     */
    public void testDeleteCsv() {
        final CsvReadWrite csv = new CsvReadWriteImpl();
        for (int x = 0; x < SIZE; x++) {
            csv.deleteElement("database nr. " + x);
        }
        List<String> listDB = new ArrayList<>();
        listDB = csv.readCSV();
        assertEquals(listDB.size(), 0);
    }
}
