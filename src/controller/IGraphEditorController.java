package controller;

import com.mxgraph.view.mxGraph;

/**
 * interface for create the controller of the diagram editor.
 * @author Luigi
 *
 */
public interface IGraphEditorController {

    /**
     * method to add an entity to Editor View e saved it in background.
     * 
     * @param graph
     *            reference to view
     * @param name
     *            name of entity
     */
    void addEntity(mxGraph graph, String name);

    /**
     * method to add a relation to Editor View e saved it in background.
     * 
     * @param graph
     *            reference to view
     * @param name
     *            name of relation
     */
    void addRelation(mxGraph graph, String name);

    /**
     * method to add an attribute to Editor View e saved it in background.
     * 
     * @param graph
     *            reference to view
     * @param name
     *            name of attribute
     */
    void addAttribute(mxGraph graph, String name);

    /**
     * method to add a primary key (id) of an entity to Editor View e saved it in
     * background.
     * 
     * @param graph
     *            reference to view
     * @param name
     *            name of primary key (id)
     */
    void addId(mxGraph graph, String name);

    /**
     * method to connect two jgraph component into view and save the change in
     * background. if the two elements can't be connected the method return false.
     * 
     * @param target name of the view component the is the Target
     * @param name name of the view component the is the Source
     * @return true if the elements are connect with success false if connection of
     *         the elements failed
     */
    boolean connect(String target, String source);

    /**
     * method to set the database name.
     * 
     * @param name
     *            name of the database
     */
    void setName(String name);

    /**
     * method to delete a element from the view.
     * 
     * @param o
     *            Object to remove
     * @param graph
     *            reference to view
     */
    void cancelElement(Object o, mxGraph graph);

    /**
     * method to print in a text file(textpostgreSQL.txt) the PostgreSQL code of the
     * view diagram.
     */
    void printCode();
}
