package controller;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * Class which implements the GUIsize interface.
 * 
 * @author silviobrasini
 *
 */
public class GUIsizeImpl implements GUIsize {

    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static final int SMALL_WIDTH = screenSize.width * 1 / 6;
    private static final int SMALL_HEIGHT = screenSize.height * 1 / 5;
    private static final int MEDIUM_WIDTH = screenSize.width * 1 / 4;
    private static final int MEDIUM_HEIGHT = screenSize.height * 1 / 3;
    private static final int BIG_WIDTH = screenSize.width * 1 / 3;
    private static final int BIG_HEIGHT = screenSize.height * 1 / 2;
    private final JFrame frame;

    /**
     * Constructor of GUIsizeImpl.
     * 
     * @param frame frame to set
     */
    public GUIsizeImpl(final JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void setSize(final int size) {
        switch(size) {
        case 1:
            this.frame.setMinimumSize(new Dimension(SMALL_WIDTH, SMALL_HEIGHT));
            break;
        case 2:
            this.frame.setMinimumSize(new Dimension(MEDIUM_WIDTH, MEDIUM_HEIGHT));
            break;
        case 3:
            this.frame.setMinimumSize(new Dimension(BIG_WIDTH, BIG_HEIGHT));
            break;
        default:
            this.frame.setMinimumSize(new Dimension(MEDIUM_WIDTH, MEDIUM_HEIGHT));
        }
    }
}
