package controller;

/**
 * Interface of GUIsizeImpl.
 * 
 * @author silviobrasini
 *
 */
public interface GUIsize {

    /**
     * Set the minimum frame size.
     * 
     * @param size 1 for small size, 2 for medium size and 3 for big size
     */
    void setSize(int size);
}
