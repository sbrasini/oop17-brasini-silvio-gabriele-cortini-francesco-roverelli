package controller;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import java.util.List;
import java.util.Locale;

/**
 * Class for load libraries from a determined folder.
 * 
 * @author silviobrasini
 *
 */
public class LoadLibrariesFromFolderImpl implements LoadLibrariesFromFolder {

    private final File folder;
    private final List<String> fileList;

    /**
     * Constructor of LoadLibrariesFromFolder.
     * 
     * @param path path of the folder
     */
    public LoadLibrariesFromFolderImpl(final String path) {
        this.fileList = new ArrayList<>();
        this.folder = new File(path);
        this.createFileList();
        this.loadLibrary();
    }

    /**
     * Create a list of .jar files which are located inside a folder.
     * 
     * @param p path
     */
    private void createFileList() {
        final File[] listOfFiles = this.folder.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(final File dir, final String name) {
                return name.toLowerCase(Locale.ENGLISH).endsWith(".jar");
            }
        });

        assert listOfFiles != null;
        for (final File file : listOfFiles) {
            if (file.isFile()) {
                this.fileList.add(file.getAbsolutePath());
            }
        }
    }

    /**
     * Load the jar libraries.
     */
    private void loadLibrary() {
        this.fileList.forEach(e -> {
            try {
                final URLClassLoader loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
                final URL url = new URL("jar:file:" + e.toString() + "!/");
                final Method method = URLClassLoader.class.getDeclaredMethod("addURL",
                        new Class[] { URL.class });
                method.setAccessible(true);
                method.invoke(loader, new Object[] { url });

            } catch (final java.lang.NoSuchMethodException | java.lang.IllegalAccessException
                    | java.net.MalformedURLException | java.lang.reflect.InvocationTargetException ex) {
                System.out.println(ex.getMessage());
            }
        });
    }
}
