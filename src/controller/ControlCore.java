package controller;

import java.util.List;
import javax.swing.JFrame;

import model.QueryType;

/**
 * interface with all method for, get list of string from file, get list of
 * database,table name or tuple.
 * 
 * @author Francesco
 *
 */
public interface ControlCore {

    /**
     * set language from language file ( Select Language).
     * 
     * @param selectedLengauge
     *            expected for example it_IT
     */
    void setLanguage(String selectedLengauge);

    /**
     * return the translated word.
     * 
     * @param string
     *            standard string
     * @return translated string
     */
    String interpreterText(String string);

    /**
     * return the translated sql text.
     * 
     * @param text
     *            standard string
     * @return translated string
     */
    String interpreterSql(String text);

    /**
     * return list of all visible database.
     * 
     * @return list of all database
     */
    List<String> getDatabaseList();

    /**
     * after set the database name, return list of all table.
     * 
     * @return list of all table in a database
     */
    List<String> getTableList();

    /**
     * after set the database name, return list of all column list.
     * 
     * @param list
     *            list of one element: the table name
     * @return list of all column in a table
     */
    List<String> getColumnsList(List<String> list);

    /**
     * set all parameters of the database.
     * 
     * @param pathDB
     *            path of database, example localhost
     * @param nameDB
     *            name of database
     * @param user
     *            user name
     * @param password
     *            password of database *
     * @param typeDB
     *            type of database, example postgres *
     * @param description
     *            path of jar file
     * @return result of connection
     */
    Boolean setDbInfo(String pathDB, String nameDB, String user, String password, String typeDB, String description);

    /**
     * Execute a query.
     * 
     * @param queryType enum, type of query
     * @param list list of string parameter
     * @return return null if the query went well, else return an error message
     */
    String executeQuery(QueryType queryType, List<String> list);

    /**
     * Set the minimum gui dimension. Pass 1 to set small size, 2 to medium and 3
     * for the biggest.
     * 
     * @param frame
     *            the frame to set
     * @param size
     *            the size to set
     */
    void setGuiSize(JFrame frame, int size);

    /**
     * Get the configuration of a database from the csv file.
     * 
     * @param dbDescription
     *            get the configuration with this description
     * @return a list with the configuration for the database
     */
    List<String> getDatabaseConfig(String dbDescription);

    /**
     * Get the list with the descriptions of all databases saved into the csv file.
     * 
     * @return a list of database descriptions
     */
    List<String> getDatabaseNameList();

    /**
     * Delete an element of the csv file.
     * 
     * @param elem
     *            element to delete
     */
    void deleteElemCsv(String elem);

    /**
     * get the path of res folder.
     * 
     * @return string of res folder.
     */
    String getResPath();

    /**
     * Different for Window and Linux, file separator is a slash or back-slash. ask
     * to operating system.
     * 
     * @return the correct file separator
     */
    String getFileSep();

    /**
     * list of string form a txt, filter with a regex.
     * 
     * @param path
     *            of txt
     * @param b
     *            for applying default regex
     * @param regex
     *            regular expression, for filter character
     * @return the list of available language
     */
    List<String> getListRegex(String path, boolean b, String regex);

    /**
     * method for compose the query.
     * 
     * @param queryType
     *            enum
     * @param list
     *            list of string, for compose query
     * @return full composed query
     */
    String getQueryCompose(QueryType queryType, List<String> list);

    /**
     * Return the exception generated by a catch.
     * 
     * @return the exception
     */
    Exception getErr();

    /**
     * Set the exception error.
     * 
     * @param err
     *            exception
     */
    void setErr(Exception err);
}