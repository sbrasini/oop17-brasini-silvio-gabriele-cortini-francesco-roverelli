package view;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import model.AttibutePane;
import model.AttibutePaneImpl;
import model.QueryType;

/**
 * GUI for create new table in the selected database.
 * 
 * @author Francesco
 *
 */
public class CreateTable {

    private final JFrame frame = new JFrame();
    private final JButton ok = new JButton("ok");
    private final JButton add = new JButton();

    private final JPanel mainPane = new JPanel();
    private final JLabel tableNameLabel = new JLabel();
    private final JTextField tableNameTextField = new JTextField();

    private final JPanel attributoPane = new JPanel();
    private final List<AttibutePane> attributoPaneList = new LinkedList<>();

    // ritorno a scelta creazione db, creazione relazione, interrogazione, gestione
    // tuple(inserimento,modifica,cancellazione)

    /**
     * Constructor.
     * 
     * @param cc
     *            ControlCore for language and other
     */
    public CreateTable(final ControlCore cc) {
        // TODO Auto-generated constructor stub
        cc.setGuiSize(frame, 1);
        this.mainPane.setLayout(new BoxLayout(this.mainPane, BoxLayout.Y_AXIS));
        this.attributoPane.setLayout(new BoxLayout(this.attributoPane, BoxLayout.Y_AXIS));
        this.tableNameLabel.setText(cc.interpreterText("Table_Name"));

        cc.interpreterText("Attribute_Name");
        this.add.setText(cc.interpreterText("Add"));

        this.add.addActionListener(e -> {
            this.attributoPaneList.add(this.getAttibutePane(cc));
            this.reloadMainPane();
        });
        this.ok.addActionListener(e -> {
            final String error;
            final List<String> list = new LinkedList<>();
            list.add(this.tableNameTextField.getText());
            this.attributoPaneList.forEach(ap -> ap.getListAtr().forEach(list::add));
            list.remove(list.size() - 1);
            error = cc.executeQuery(QueryType.CREATE_TABLE, list);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
        });

        // dopo il nome della tabella, ci va un elenco di attributi
        // di cui almeno uno sia chiave primaria
        // dentro a un JScrollPane; ex:
        // final JScrollPane scroll = new JScrollPane(textArea); // Pannello con barra
        // l' attributo sarà formato da:
        // nome,tipo,PRIMARY KEY,NOT NULL ;tipo(INT-CHAR) ;se il tipo è una stringa va
        // messa la lunghezza es(ADDRESS CHAR(50))-(City varchar(255))

        // e dopo l'attibuto ci andrà una "X" se si vuole rimuovere
        this.frame.setTitle(cc.interpreterText("Create_Table"));
        this.reloadMainPane();
        // this.frame.pack();
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
    }

    private AttibutePane getAttibutePane(final ControlCore cc) {
        // TODO Auto-generated method stub
        final AttibutePane ap = new AttibutePaneImpl(cc);
        final JButton deleteAttPane = new JButton("X");
        deleteAttPane.addActionListener(e -> {
            this.attributoPaneList.remove(ap);
            this.reloadMainPane();
        });
        ap.getPanel().add(deleteAttPane);
        return ap;
    }

    private void reloadMainPane() {
        // TODO Auto-generated method stub
        this.attributoPane.removeAll();
        this.mainPane.removeAll();
        this.attributoPaneList.forEach(a -> this.attributoPane.add(a.getPanel()));

        this.mainPane.add(this.tableNameLabel);
        this.mainPane.add(this.tableNameTextField);
        this.mainPane.add(this.attributoPane);
        this.mainPane.add(this.add);
        this.mainPane.add(this.ok);
        this.frame.setContentPane(this.mainPane);
        this.frame.pack();
    }
}
