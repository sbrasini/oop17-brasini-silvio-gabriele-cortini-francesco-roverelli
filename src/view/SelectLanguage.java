package view;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import controller.ControlCore;
import model.ModelLanguage;
import model.ModelLanguageImpl;

/**
 * Choice of the language.
 * 
 * @author silviobrasini , Francesco
 *
 */
public class SelectLanguage {

    private final JFrame frame = new JFrame();
    private final JComboBox<String> combo = new JComboBox<>();
    private final ModelLanguage ml;

    private void resetCombo() {
        this.combo.removeAllItems();
        this.ml.getListLanguage().forEach(this.combo::addItem);
        if (!this.ml.getListLanguage().isEmpty()) {
            this.combo.setSelectedIndex(0);
        }
        this.frame.pack();
    }

    /**
     * 
     * @param cc
     *            control core
     * @param main 
     */
    public SelectLanguage(final ControlCore cc, final Main main) {

        cc.setGuiSize(this.frame, 1);
        this.ml = new ModelLanguageImpl(cc);
        this.resetCombo();

        this.frame.setLocationByPlatform(true);

        this.frame.setTitle("SelectLanguage");
        final JButton button = new JButton("ok");
        button.addActionListener(e -> {
            cc.setLanguage(this.ml.getSelectedLangauge(this.combo.getSelectedIndex()));
            main.reloadText(cc);
        });
        final JPanel pane = new JPanel();
        final FlowLayout fl = new FlowLayout();
        fl.setAlignment(FlowLayout.LEFT);
        pane.setLayout(fl);
        pane.add(this.combo);
        pane.add(button);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setContentPane(pane);
        this.frame.pack();
        this.frame.setVisible(true);
    }
}
