package view;

import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.ControlCore;
import model.QueryType;

/**
 * gui where the expert user can execute free query.
 * 
 * @author Francesco
 *
 */
public class FreeQueryGui {

    private final JFrame frame = new JFrame();
    private final JCheckBox checkBox = new JCheckBox();
    private final JTextArea area = new JTextArea();
    private final JButton execute = new JButton();
    private final List<String> queryList = new LinkedList<>();
    private final JPanel pane = new JPanel();

    /**
     * the gui for the expert guy.
     * 
     * @param cc
     *            ControlCore
     */
    public FreeQueryGui(final ControlCore cc) {
        // TODO Auto-generated constructor stub
        cc.setGuiSize(this.frame, 2);
        this.pane.setLayout(new BoxLayout(this.pane, BoxLayout.Y_AXIS));
        this.checkBox.setText(cc.interpreterText("SELECT"));
        this.execute.setText(cc.interpreterText("Exequte_Query"));
        this.execute.addActionListener(e -> {
            this.queryList.add(this.area.getText());
            final String error;
            if (this.checkBox.isSelected()) {
                error = cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION, this.queryList);
            } else {
                error = cc.executeQuery(QueryType.FREE_QUERY_UPDATE, this.queryList);
            }
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
            this.queryList.clear();
        });
        this.area.setText("SELECT table_schema,table_name\r\n" + "FROM information_schema.tables\r\n"
                + "WHERE table_schema = 'public'\r\n" + "OR table_schema = 'PUBLIC'\r\n"
                + "ORDER BY table_schema,table_name;");
        this.pane.add(this.checkBox);
        this.pane.add(this.area);
        this.pane.add(this.execute);
        this.frame.setContentPane(this.pane);
        this.frame.setTitle(cc.interpreterText("Free_Query"));
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
    }
}
