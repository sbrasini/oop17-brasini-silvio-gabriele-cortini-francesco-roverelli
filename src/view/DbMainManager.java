package view;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import controller.ControlCore;
import view.FreeQueryGui;

/**
 * gui to manage the database.
 * 
 * @author Francesco
 *
 */
public class DbMainManager {

    private final JFrame frame = new JFrame();

    // creare, modifica, distrugge database
    private final JButton dbGui = new JButton();
    // creare tabelle
    private final JButton createTableGui = new JButton();
    // modifica o distrugge tabelle
    private final JButton adTableGui = new JButton();
    // inserisce tuple
    private final JButton insertTupleGui = new JButton();
    // modifica o distrugge tuple
    private final JButton udTupleGui = new JButton();
    // interrogazione
    private final JButton selectGui = new JButton();
    // query libere per l'utente
    private final JButton freeQueryGui = new JButton();
    private static final int BUTTON_NUMBER = 7;

    /**
     * constructor of DbMainManager.
     * 
     * @param cc
     *            ControlCore
     */
    public DbMainManager(final ControlCore cc) {
        // TODO Auto-generated constructor stub
        cc.setGuiSize(this.frame, 1);
        final JPanel pane = new JPanel(new GridLayout(DbMainManager.BUTTON_NUMBER, 1));
        this.dbGui.setText(cc.interpreterText("Database_Manager"));
        this.createTableGui.setText(cc.interpreterText("Table_Create"));
        this.adTableGui.setText(cc.interpreterText("Table_Alter_Drop"));

        this.insertTupleGui.setText(cc.interpreterText("Tuple_Insert"));
        //update/delete rows of a table
        this.udTupleGui.setText(cc.interpreterText("Tuple_Update_Delete"));
        this.selectGui.setText(cc.interpreterText("Query_Manager"));
        this.freeQueryGui.setText(cc.interpreterText("Free_Query_Manager"));
        this.dbGui.addActionListener(e -> {
            new DatabaseManagerGui(cc);
        });
        this.createTableGui.addActionListener(e -> {
            new CreateTable(cc);
        });
        this.adTableGui.addActionListener(e -> {
            new DeleteTable(cc);
        });
        this.insertTupleGui.addActionListener(e -> {
            new InsertInto(cc);
        });
        this.udTupleGui.addActionListener(e -> {
            new DeleteTuple(cc);
        });
        this.selectGui.addActionListener(e -> {
            new QueryInterrogation(cc);
        });
        this.freeQueryGui.addActionListener(e -> {
            new FreeQueryGui(cc);
        });

        pane.add(this.dbGui);
        pane.add(this.createTableGui);
        pane.add(this.adTableGui);
        pane.add(this.insertTupleGui);
        pane.add(this.udTupleGui);
        pane.add(this.selectGui);
        pane.add(this.freeQueryGui);
        this.frame.setContentPane(pane);
        this.frame.setTitle(cc.interpreterText("Database_Main_Manager"));
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
    }

}
