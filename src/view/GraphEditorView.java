
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraph;
import controller.GraphEditorController;
import controller.IGraphEditorController;

/**
 * 
 * @author Luigi
 *
 */
public class GraphEditorView extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private mxGraph graph;
    private mxGraphComponent graphComponent;
    private JButton name;
    private JButton query;
    private JButton relation;
    private JButton entity;
    private JButton attribute;
    private JButton delete;
    private JButton iD;
    private JTextField txtbox;
    private Object cell;
    private IGraphEditorController controller;
    private Boolean closed;

    /**
     * Constructor of GraphEditorView.
     */
    public GraphEditorView() {
        super("Database Diagram Editor");
        name = new JButton("nome DB");
        relation = new JButton("relazione");
        entity = new JButton("entita");
        attribute = new JButton("attributo");
        delete = new JButton("cancella");
        iD = new JButton("ID");
        query = new JButton("query code");
        txtbox = new JTextField();
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setResizable(false);
        controller = new GraphEditorController();
        initGui();
    }

    private void initGui() {
        setSize(800, 600);
        setLocationRelativeTo(null);
        graph = new mxGraph();
        graphComponent = new mxGraphComponent(graph);
        graphComponent.setPreferredSize(new Dimension(773, 490));
        graphComponent.setBorder(BorderFactory.createLineBorder(Color.black));

        txtbox.setPreferredSize(new Dimension(580, 25));
        entity.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.addEntity(graph, txtbox.getText());
                txtbox.setText("");
            }
        });
        name.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!txtbox.equals("")) {
                    controller.setName(txtbox.getText());
                    txtbox.setText("");
                }
            }
        });
        attribute.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!txtbox.equals("")) {
                    controller.addAttribute(graph, txtbox.getText());
                    txtbox.setText("");
                }
            }
        });

        iD.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!txtbox.equals("")) {
                    controller.addId(graph, txtbox.getText());
                    txtbox.setText("");
                }
            }
        });

        relation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!txtbox.equals("")) {
                    controller.addRelation(graph, txtbox.getText());
                    txtbox.setText("");
                }
            }
        });

        delete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (cell != null) {
                    controller.cancelElement(cell, graph);
                    cell = null;
                }
            }
        });

        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                cell = graphComponent.getCellAt(e.getX(), e.getY());
            }
        });

        graphComponent.getConnectionHandler().addListener(mxEvent.CONNECT, new mxIEventListener() {
            public void invoke(Object sender, mxEventObject evt) {
                mxCell connt = (mxCell) evt.getProperty("cell");
                String t = connt.getTarget().getValue().toString();
                String s = connt.getSource().getValue().toString();
                if (!controller.connect(t, s)) {

                }
            }
        });

        query.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                controller.printCode();
                graph.removeCells(graph.getChildVertices(graph.getDefaultParent()));
            }
        });

        getContentPane().add(delete);
        getContentPane().add(name);
        getContentPane().add(relation);
        getContentPane().add(attribute);
        getContentPane().add(iD);
        getContentPane().add(entity);
        getContentPane().add(query);
        getContentPane().add(txtbox);
        getContentPane().add(graphComponent);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                setClosed(true);
            }
        });
        setClosed(false);

    }
    
    public Boolean isClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

}