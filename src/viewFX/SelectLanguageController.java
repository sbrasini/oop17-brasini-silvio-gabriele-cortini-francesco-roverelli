package viewFX;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;

/**
 * 
 * @author ChristopherRoverelli
 *
 */
public class SelectLanguageController extends AnchorPane implements Initializable {
    // elenco controlli grafici
    @FXML
    private ChoiceBox<String> chbLanguage;

    @FXML
    private Button btnOKLanguage;

    @FXML
    private Label lblLanguage;

    @FXML
    private AnchorPane pnlSelectLanguage;

    // variabile per il collegamento con l'applicazione principale
    private MainApp mainApp;

    /**
     * inizializzazione di alcuni controlli.
     * 
     * @param location
     *            url file FXML
     * 
     * @param resource
     *            ResourceBundle
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.chbLanguage.setTooltip(new Tooltip("Select the language"));
    }

    /**
     * collegamento con applicazione principale e carica l'elenco delle lingue
     * supportate.
     * 
     * @param mainApp
     *            applicazione lancio UI
     */
    public void setApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.chbLanguage.setItems(FXCollections.observableList(this.mainApp.getML().getListLanguage()));
        this.chbLanguage.getSelectionModel().select(0);
    }

    /*
     * imposta la lingua selezionata
     */
    @FXML
    private void selectLanguage() {
        this.mainApp.setLanguage(
                this.mainApp.getML().getSelectedLangauge(chbLanguage.getSelectionModel().getSelectedIndex()));
        this.mainApp.getCC().setLanguage(
                this.mainApp.getML().getSelectedLangauge(chbLanguage.getSelectionModel().getSelectedIndex()));
        this.mainApp.editorDB();
    }

}
