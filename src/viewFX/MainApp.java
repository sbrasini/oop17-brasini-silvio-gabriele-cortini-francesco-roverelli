package viewFX;

import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

import controller.ControlCore;
import controller.ControlCoreImpl;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.ModelLanguage;
import model.ModelLanguageImpl;

/**
 * 
 * @author ChristopherRoverelli
 *
 */
public class MainApp extends Application {
    //Group di componenti grafici da caricare sul primaryStage
	private static Group root;
	//contenitore principale del UI
	private Stage primaryStage;
	//bundle suporto lingua
	ResourceBundle bundle = ResourceBundle.getBundle("LanguagePack.MyLabels",Locale.getDefault());
	//controllo principale per l'accesso e la modifica ai database
	private ControlCore cc;
	//per il recupero delle lingue supportate
	private ModelLanguage ml;
	//indica se la connessione è andata a buon fine
	private Boolean connDB = false;
	
    @Override
    public void start(final Stage primaryStage) {
        cc = new ControlCoreImpl();
        ml = new ModelLanguageImpl(cc);
        if(Locale.getDefault().getLanguage().equals("it"))
        {
            cc.setLanguage(ml.getSelectedLangauge(0));
        }
        else {
            cc.setLanguage(ml.getSelectedLangauge(1));
        }
        
        this.primaryStage = primaryStage;
    	primaryStage.setResizable(false);
    	primaryStage.setTitle("DB Editor");
        primaryStage.setScene(new Scene(createContent()));
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
        primaryStage.show();
    }
    /*
     * caricamento prima volta del UI DBEditor e assegnamento root al primaryStage
     */
    private Parent createContent() {
        gotoTypeEditor();
        return root;
    }
    /*
     * ritorno il valore di connDB per sapere se la connessione è andata
     * a buon fine
     */
    public Boolean getConnectDB() {
        return connDB;
    }
    /*
     * setto connDB a true per indicare che la connessione è andata a buon fine
     * altrimenti a false
     */
    public void setConnectDB(final Boolean connectDB) {
        this.connDB = connectDB;
    }
    /*
     * cambio UI in DB Editor
     */
    private void gotoDBEditor() {
        try {
            DBEditorController ctrlDBE =
                (DBEditorController)replaceSceneContent("DBEditor.fxml");
            primaryStage.sizeToScene();
            ctrlDBE.setApp(this);
        } catch (Exception ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }
    /*
     * cambio UI in SelectLanguage
     */
    private void gotoLanguage() {
        try {
            SelectLanguageController ctrlSL =
                (SelectLanguageController)replaceSceneContent("SelectLanguage.fxml");
            primaryStage.sizeToScene();
            ctrlSL.setApp(this); 
        } catch (Exception ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }
    
    /*
     * cambio UI in TypeEditor
     */
    private void gotoTypeEditor() {
        try {
            TypeEditorController ctrlTE =
                (TypeEditorController)replaceSceneContent("TypeEditor.fxml");
            primaryStage.sizeToScene();
            ctrlTE.setApp(this); 
        } catch (Exception ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }
    
    /*
     * cambio UI in ConnectDatabase
     */
    private void gotoConnectDatabase() {
        try {
            ConnectDatabaseController ctrlLR =
                (ConnectDatabaseController)replaceSceneContent("ConnectDatabase.fxml");
            primaryStage.sizeToScene();
            ctrlLR.setApp(this);
        } catch (Exception ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }
    /*
     * sostituisco il contenuto di root con una nuova UI e aggiorno la schermata
     */
    private Initializable replaceSceneContent(final String fxml) throws Exception {
        final FXMLLoader loader = new FXMLLoader();
        final InputStream in = MainApp.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(MainApp.class.getResource(fxml));
        loader.setResources(bundle);
        AnchorPane page;

        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        }
        root.getChildren().clear();
        root.getChildren().add(page);

        return (Initializable)loader.getController();
    }
    /*
     * setto la lingua nel bundle
     */
    public void setLanguage(final String lang) {
    	bundle = ResourceBundle.getBundle("LanguagePack.MyLabels",new Locale(lang));
    }
    /*
     * richiamo UI SelectLanguage
     */
    public void selectLang() {
        this.gotoLanguage();
    }
    /*
     * richiamo UI ConnectDatabase
     */
    public void connectDB() {
        this.gotoConnectDatabase();
    }
    /*
     * richiamo UI DBEditor
     */
    public void editorDB() {
        this.gotoDBEditor();
    }
   
    public void typeEditor() {
        this.gotoTypeEditor();
    }
    
    /*
     * ritorna lo stage principale
     */
    public Stage getPrimaryStage() {
        return this.primaryStage;
    }
    /*
     * ritorna il controlcore
     */
    public ControlCore getCC() {
        return this.cc;
    }
    /*
     * ritorna il modellanguage
     */
    public ModelLanguage getML() {
        return this.ml;
    }
    
	public static void main(final String[] args) {
	    root = new Group();
		Application.launch(args);
	}

    
	
}