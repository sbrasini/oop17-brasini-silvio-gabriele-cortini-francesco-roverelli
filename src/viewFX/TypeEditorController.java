package viewFX;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import view.GraphEditorView;
import view.Main;

public class TypeEditorController extends AnchorPane implements Initializable {

    @FXML
    private Button btnJavaFX;

    @FXML
    private Button btnSwing;

    @FXML
    private Button btnGraph;

    // variabile per il collegamento con l'applicazione principale
    private MainApp mainApp;
    GraphEditorView gf;
    Main swing;

    /**
     * collegamento con applicazione principale e carica l'elenco delle lingue
     * supportate.
     * 
     * @param mainApp
     *            applicazione lancio UI
     */
    public void setApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO Auto-generated method stub

    }

    @FXML
    private void javaFX() {
        
        if (this.swing != null) {
            if(!this.swing.isClosed()) {
                this.swing.close();
                this.swing = null;
            } 
        }
        
        if (this.gf != null) {
            if(!this.gf.isClosed()) {
                this.gf.setVisible(false);
                this.gf.setClosed(true);
                this.gf.dispose();
                this.gf = null;
            }
            
        }
        this.mainApp.editorDB();
        
    }

    @FXML
    private void swing() {
        if (this.gf != null) {
            if(!this.gf.isClosed()) {
                this.gf.setVisible(false);
                this.gf.setClosed(true);
                this.gf.dispose();
                this.gf = null;
            }
            
        }
        if (this.swing == null || this.swing.isClosed()) {
            this.swing = new Main(this.mainApp.getCC());
        }
        
    }

    @FXML
    private void graph() {
        if (this.swing != null) {
            if(!this.swing.isClosed()) {
                this.swing.close();
                this.swing = null;
            } 
        }
        if (this.gf == null || this.gf.isClosed()) {
            this.gf = new GraphEditorView();
            this.gf.setVisible(true);
        }

    }

}
