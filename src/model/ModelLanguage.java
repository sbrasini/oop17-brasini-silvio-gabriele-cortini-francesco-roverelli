package model;

import java.util.List;

/**
 * interface.
 * @author Francesco
 *
 */
public interface ModelLanguage {
    /**
     * example: English,Italian,Frances.
     * 
     * @return list of language.
     */
    List<String> getListLanguage();

    /**
     * example: choice of italian -> it_IT.
     * 
     * @param selectedIndex
     *            index of LanguageList.
     * @return Language_Country.
     */
    String getSelectedLangauge(int selectedIndex);

}
