package model;

import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controller.ControlCore;

/**
 * implemented class for an easy form of attribute.
 * @author Francesco
 *
 */
public class AttibutePaneImpl implements AttibutePane {

    private final JPanel attributo;
    private final JCheckBox primaryKeyBox;
    private final JCheckBox notNullBox;
    private final JTextField attributoTextField;
    private final List<String> typecc;
    private final ControlCore cc;
    private final JComboBox<String> typeComboBox;
    private final JTextField charLong;

    private static final int MAX_EDIT = 255;

    /**
     * gui creator.
     * 
     * @param cc
     *            ControlCore
     */
    public AttibutePaneImpl(final ControlCore cc) {
        // TODO Auto-generated constructor stub
        this.cc = cc;
        this.typecc = this.cc.getListRegex("/Conf/Attribute.txt", false, "");
        this.attributo = new JPanel();
        attributo.setLayout(new GridLayout(1, 1));
        final JLabel nomeAttributo = new JLabel(cc.interpreterText("Attribute_Name"));
        this.attributoTextField = new JTextField();

        this.primaryKeyBox = new JCheckBox(SqlDefault.PRIMARY_KEY.toString());

        this.notNullBox = new JCheckBox(SqlDefault.NOT_NULL.toString());

        this.typeComboBox = new JComboBox<>();
        this.charLong = new JTextField(cc.interpreterText(SqlDefault.MAX_EDIT.toString()));

        for (final String string : this.typecc) {
            this.typeComboBox.addItem(string.toUpperCase().replaceAll("#", ""));
        }

        if (!this.typecc.isEmpty()) {
            this.typeComboBox.setSelectedIndex(0);
        }
        charLong.setEditable(this.needNumber(this.typeComboBox.getSelectedIndex()));
        this.typeComboBox.addActionListener(e -> {
            charLong.setEditable(this.needNumber(this.typeComboBox.getSelectedIndex()));
        });
        this.primaryKeyBox.addActionListener(e -> {
            if (this.primaryKeyBox.isSelected()) {
                this.notNullBox.setEnabled(false);
                this.notNullBox.setSelected(true);
            } else {
                this.notNullBox.setEnabled(true);
            }
        });

        this.attributo.add(nomeAttributo);
        this.attributo.add(this.attributoTextField);
        this.attributo.add(this.primaryKeyBox);
        this.attributo.add(this.typeComboBox);
        this.attributo.add(this.charLong);
        this.attributo.add(this.notNullBox);
    }

    private boolean needNumber(final int selectedIndex) {
        // TODO Auto-generated method stub
        return this.typecc.get(selectedIndex).contains("#");
    }

    @Override
    public JPanel getPanel() {
        // TODO Auto-generated method stub
        return this.attributo;
    }

    @Override
    public List<String> getListAtr() {
        // TODO Auto-generated method stub
        final List<String> list = new LinkedList<>();
        list.add(this.attributoTextField.getText().replaceAll("\\s+", ""));
        list.add(cc.interpreterSql(this.typecc.get(this.typeComboBox.getSelectedIndex()).toUpperCase().replaceAll("#", "")));
        if (this.needNumber(this.typeComboBox.getSelectedIndex())) {
            list.add(this.cc.interpreterSql(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString()));
            int i;
            try {
                i = Integer.parseInt(this.charLong.getText());
            } catch (Exception e) {
                // TODO: handle exception
                i = AttibutePaneImpl.MAX_EDIT;
            }
            if (i <= 0 || i > Integer.parseInt(this.cc.interpreterSql(SqlDefault.MAX_EDIT.toString()))) {
                i = AttibutePaneImpl.MAX_EDIT;
            }
            this.charLong.setText(String.valueOf(i));
            list.add(this.charLong.getText());
            list.add(this.cc.interpreterSql(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString()));
        }
        if (this.primaryKeyBox.isSelected()) {
            list.add(this.cc.interpreterSql(SqlDefault.PRIMARY_KEY.toString()));
        }
        if (this.notNullBox.isSelected()) {
            list.add(this.cc.interpreterSql(SqlDefault.NOT_NULL.toString()));
        }
        list.add(this.cc.interpreterSql(SqlDefault.SQL_COMMA.toString()));
        return list;
    }

}
