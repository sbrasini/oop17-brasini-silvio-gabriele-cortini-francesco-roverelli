package model;

import java.util.List;

/**
 * Interfaccia che modella un oggetto di tipo Database.
 * 
 * @author silviobrasini
 *
 */
public interface Database {

    /** 
     * @return path of the database
     */
    String getPathDB();
    /** 
     * @return name of the database
     */
    String getNameDB();
    /** 
     * @return username of the database
     */
    String getUser();
    /** 
     * @return password of the database
     */
    String getPassw();
    /** 
     * @return library path of the database
     */
    //String getPathLib();
    /** 
     * @return type of the database
     */
    String getTypeDB();
    /** 
     * @return description of the database
     */
    String getDescription();

    /**
     * Execute a query.
     * 
     * @param queryType type of the query
     * @param query the query
     * @return return null if the query went well, else return an error message
     */
    String executeQuery(QueryType queryType, String query);

    /**
     * Try if it is possible to establish a connection with the database.
     * 
     * @return true se connessione avvenuta senza problemi, false altrimenti
     */
    Boolean tryConnection();

    /**
     * Get list of all database.
     * 
     * @param queryType QueryType
     * @param query string of query
     * @return list of all databases
     */
    List<String> executeQueryListReturn(QueryType queryType, String query);

}
