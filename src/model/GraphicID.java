package model;
import java.util.Hashtable;

import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;

/**
 * 
 * @author Luigi
 *
 */
public class GraphicID implements IGraphicID {

    private String iD;
    private String name;

    /**
     * Constructor of GraphicID.
     * @param iD
     * @param name
     */
    public GraphicID(String iD, String name) {
        this.iD = iD;
        this.name = name;
    }

    @Override
    public String getID() {
        return iD;
    }

    @Override
    public void setID(String iD) {
        this.iD = iD;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Object figreAtt(mxGraph graph) {

        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_DOUBLE_ELLIPSE);
        style.put(mxConstants.STYLE_OPACITY, 50);
        style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
        graph.getStylesheet().putCellStyle("ROUNDED", style);

        Object parent = graph.getDefaultParent();
        return (Object) graph.insertVertex(parent, this.iD, this.name, 20, 20, 80, 30, "ROUNDED");
    }
}
