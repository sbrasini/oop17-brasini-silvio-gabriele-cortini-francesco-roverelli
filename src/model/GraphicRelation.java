package model;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;

/**
 * 
 * @author Luigi
 *
 */
public class GraphicRelation implements IGraphicRelation {

    private Integer iD;
    private String name;
    private List<String> connectEntityId;
    private List<String> attributes;

    /**
     * Constructor of GraphicRelation.
     * @param id
     * @param name
     */
    public GraphicRelation(Integer id, String name) {
        this.iD = id;
        this.name = name;
        this.connectEntityId = new LinkedList<>();
        this.attributes = new LinkedList<>();
    }

    @Override
    public Integer getID() {
        return iD;
    }

    @Override
    public void setID(Integer iD) {
        this.iD = iD;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<String> getAttributes() {
        return attributes;
    }

    @Override
    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public void addConnectEntityId(String entity) {
        this.connectEntityId.add(entity);
    }

    @Override
    public void setRightConnectEntityId(List<String> connectEntityId) {
        this.connectEntityId = connectEntityId;
    }

    @Override
    public List<String> getConnectEntityId() {
        return connectEntityId;
    }

    @Override
    public void addAttributes(String attribute) {
        this.attributes.add(attribute);
    }

    @Override
    public void addAttributes(LinkedList<String> attributes) {
        this.attributes.addAll(attributes);
    }

    @Override
    public Object figure(mxGraph graph) {
        Object parent = graph.getDefaultParent();
        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RHOMBUS);
        style.put(mxConstants.STYLE_OPACITY, 60);
        style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
        graph.getStylesheet().putCellStyle("RHOMBUS", style);
        return (Object) graph.insertVertex(parent, this.iD.toString(), this.name, 30, 40, 100, 80, "RHOMBUS");
    }
}
