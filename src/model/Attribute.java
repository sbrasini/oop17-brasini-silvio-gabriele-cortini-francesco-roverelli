/**
 * 
 */
package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

/**
 * @author ChristopherRoverelli
 *
 */
public interface Attribute {
    
    public StringProperty nameProperty();
    
    public String getName();
    
    public void setName(String name);
    
    public BooleanProperty primaryKeyProperty();
    
    public Boolean getPrimaryKey();
    
    public void setPrimaryKey(Boolean key);
    
    public StringProperty typeProperty();
    
    public String getType();
    
    public void setType(String type);
    
    public IntegerProperty lenghtProperty();
    
    public Integer getLenght();
    
    public void setLenght(int lenght);
    
    public BooleanProperty notNullProperty();
    
    public Boolean getNotNull();
    
    public void setNotNull(Boolean key);
    
    

}
